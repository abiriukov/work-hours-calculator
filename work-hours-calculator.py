#!/usr/bin/python

import datetime as dt
import weakref

import wx
from wx.lib.masked import timectrl


def get_days_in_month(month: str) -> int:
    if month in ["January", "March", "May", "July", "August", "October", "December"]:
        return 31
    elif month in ["April", "June", "September", "November"]:
        return 30
    else:
        current_year = dt.datetime.now().year
        if (current_year % 4 == 0) and (current_year % 100 != 0 or current_year % 400 == 0):
            return 29
        else:
            return 28


class MainWindow(wx.Frame):
    input_instances = set()

    def __init__(self, *args, **kwargs):
        wx.Frame.__init__(self, *args, **kwargs)

        # Initially create inputs for 31 days
        self.days_in_month = 31

        self.panel = wx.Panel(self, wx.ID_ANY)

        # Month selector
        self.label_month = wx.StaticText(self.panel, wx.ID_ANY, "Month")
        self.month_list = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                           'October', 'November', 'December']
        self.current_month = dt.datetime.now().strftime('%B')
        self.select_month = wx.ComboBox(self.panel, choices=self.month_list)
        self.Bind(wx.EVT_COMBOBOX, self.on_combobox_change, self.select_month)

        # Sizer declaration
        self.month_selector_sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.main_sizer = wx.BoxSizer(wx.VERTICAL)
        self.grid_sizer = wx.GridSizer(rows=5, cols=7, hgap=5, vgap=5)
        self.total_sizer = wx.BoxSizer(wx.HORIZONTAL)

        # Month selector sizer
        self.month_selector_sizer.Add(self.label_month, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)
        self.month_selector_sizer.Add(self.select_month, 0, wx.ALL, 5)

        # Days
        self.generate_days_inputs()

        # Total sizer
        self.label_total = wx.StaticText(self.panel, wx.ID_ANY, "Total: 00:00")
        self.total_sizer.Add(self.label_total, 0, wx.ALL | wx.ALIGN_CENTER_VERTICAL, 5)

        # Main sizer
        self.main_sizer.Add(self.month_selector_sizer, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)
        self.main_sizer.Add(self.grid_sizer, 0, wx.ALL | wx.EXPAND, 5)
        self.main_sizer.Add(self.total_sizer, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.SetSizeHints(550, 500)

        self.panel.SetSizer(self.main_sizer)
        self.main_sizer.Fit(self)

        # Set current month and hide unused days
        self.select_month.SetValue(self.current_month)
        self.days_in_month = get_days_in_month(self.current_month)
        self.hide_unused_days()

    def generate_days_inputs(self) -> None:
        # Create labels, inputs etc. for 31 days
        for day in range(1, 32):
            temp_label = wx.StaticText(self.panel, wx.ID_ANY, day.__str__())
            temp_input = timectrl.TimeCtrl(self.panel, wx.ID_ANY, value="00:00", format="24HHMM", max="24:00",
                                           limited=True)

            self.input_instances.add(weakref.ref(temp_input))

            self.Bind(timectrl.EVT_TIMEUPDATE, self.on_time_update, temp_input)

            temp_input_sizer = wx.BoxSizer(wx.VERTICAL)

            temp_input_sizer.Add(temp_label, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)
            temp_input_sizer.Add(temp_input, 0, wx.ALL | wx.EXPAND, 5)

            self.grid_sizer.Add(temp_input_sizer, 0, wx.ALIGN_CENTER)

    def hide_unused_days(self) -> None:
        counter = 1

        for child in self.grid_sizer.GetChildren():
            if counter > self.days_in_month:
                self.grid_sizer.Hide(child.Sizer)
            else:
                self.grid_sizer.Show(child.Sizer)
            counter += 1

    def reset_time(self):
        for obj in self.get_input_instances():
            obj.SetValue("00:00")

    @classmethod
    def get_input_instances(cls):
        deleted_instances = set()

        for ref in cls.input_instances:
            obj = ref()

            if obj is not None:
                yield obj
            else:
                deleted_instances.add(ref)

            cls.input_instances -= deleted_instances

    # Events
    def on_combobox_change(self, event):
        selected_month = event.GetString()

        days_in_current_month = get_days_in_month(selected_month)

        if days_in_current_month != self.days_in_month:
            self.days_in_month = days_in_current_month
            self.hide_unused_days()
            self.reset_time()

    def on_time_update(self, event):
        # Calculate the total time from all the inputs
        total_time = wx.TimeSpan(0, 0)

        for obj in self.get_input_instances():
            total_time += obj.GetValue(False, False, True)

        self.label_total.SetLabelText("Total: " + total_time.Format("%H:%M"))


if __name__ == "__main__":
    app = wx.App()
    frame = MainWindow(None, id=wx.ID_ANY, title="Work Hours Calculator").Show()
    app.MainLoop()
