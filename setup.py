from setuptools import setup

setup(
    name='work-hours-calculator',
    version='1.0',
    packages=['work-hours-calculator'],
    url='https://gitlab.com/abiriukov/work-hours-calculator',
    license='GPL',
    author='Aleksandr Biriukov',
    author_email='programmer@wildions.com',
    description='A simple work hours calculator (per month).'
)
